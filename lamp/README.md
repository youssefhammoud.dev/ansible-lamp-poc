LAMP
=========

This playbook allows you to install a web server (apache + php + custom packages)
with a test web app and a database server(mysql) with a custom user and table.

Roles
--------------

- web : [Documentation here](roles/web/README.md)
- database : [Documentation here](roles/database/README.md)

Result of the web page :

![home page](../images/home_page.jpg)

Author Information
------------------
Youssef Hammoud

