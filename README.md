# Ansible
In this repo you will find all my ansible playbooks and roles to deploy a lamp stack application in linux docker containers.

## Prerequisites
You will need the following tools:

* [Visual Studio Code](https://code.visualstudio.com/download/)
* [Docker](https://docs.docker.com/get-docker/)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html)
* [Debian os family or RedHat OS family](https://opensource.com/article/19/7/ways-get-started-linux)

## About the palybooks/roles
Please read the documentation foreach role in the [README.md](lamp/README.md) file before starting.  

## Running
### Setup your environment

You need first to create two virtual machines (the first one is used to deploy the web application and the second is used to deploy the mysql database).

To reduce the complexity we create those machines as docker containers using the docker-compose.yml file, so two steps are required :

1. On your local machine, generate an [SSH Key Pair](https://docs.oracle.com/en/cloud/cloud-at-customer/occ-get-started/generate-ssh-key-pair.html), then copy the public key generated to the variable SSH_AUTHORIZED_KEY in [.env](linux/.env) file (this enable ansible to communicate with the Managed nodes without password).

2. Run the [docker-compose.yml](linux/docker-compose.yml) file to create the two machines :
    ```
    $ cd linux && docker-compose up -d
    ```
### Run the playbook
Please follow the steps below :

- Get the IP addresses for your containers :
  ```sh
  docker ps --format "{{.Names}}" | xargs -t -n1 docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
  ```  

- Update the files [hosts](lamp/hosts), [lamp/roles/web/main.yml](lamp/roles/web/vars/main.yml), [lamp/vars/main.yml](lamp/vars/main.yml), with your IP results.

- Create a vault :
  ```sh
  cd lamp && ansible-vault create vars/mysql-users.yml
  ```
- Put the mysql sensitive data in your current vault file :
  ```yaml
  ---
  mysql_user: "admin"
  mysql_password: "secret"
  root_password: "secret"
  ```
- Run the playbook :
  ```sh
  cd lamp && ansible-playbook playbook.yml --ask-vault-pass
  ```
- Conclusion and Test:
  Our playbook is now operational on target machines of the RedHat and Debian family.
  Once your playbook is launched, visit the following page http://IP_SERVEUR_WEB:8080.
  
![ansible logo](images/ansible.png)
